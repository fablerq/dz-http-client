package com.dz

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.dz.routes.SummaryOfRoutes

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object Server extends App{
  implicit val system: ActorSystem = ActorSystem("Server")
  implicit val executor: ExecutionContext = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val SummaryOfRoutes = new SummaryOfRoutes

  Http().bindAndHandle(SummaryOfRoutes.routes, "0.0.0.0", 8000).onComplete {
    case Success(b) => println(s"Server is running at ${b.localAddress.getHostName}:${b.localAddress.getPort}")
    case Failure(e) => println(s"Could not start application: {}", e.getMessage)
  }

}
