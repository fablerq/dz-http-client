package com.dz.repositories

import java.util.concurrent.atomic.AtomicLong

import com.dz.models._

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

trait TraitUserRepository {
  def getAll(): Future[List[User]]
  def get(id: Long): Future[Option[User]]
  def create(params: UserParams): Future[User]
  def update(id: Long, params: UserParams): Future[Option[User]]
  def delete(id: Long): Future[Boolean]
}

class UserRepository extends TraitUserRepository {
  private val storage: TrieMap[Long, User] = TrieMap[Long, User]()
  private val idSeq = new AtomicLong(1)

  //Тестовые данные
  storage.put(
    idSeq.getAndIncrement(),
    User(
      id = 1,
      name =  "Leanne Graham",
      username = "Bret",
      email = "Sincere@april.biz",
      address = Address(
        street = "Kulas Light",
        suite = "Apt. 556",
        city = "Gwenborough",
        zipcode = "92998-3874",
        geo = Geo(
          lat = "-37.3159",
          lng = "81.1496"
        )
      ),
      phone = "1-770-736-8031 x56442",
      website = "hildegard.org",
      company = Company(
        name = "Romaguera-Crona",
        catchPhrase = "Multi-layered client-server neural-net",
        bs = "harness real-time e-markets"
      )
    )
  )

  storage.put(
    idSeq.getAndIncrement(),
    User(
      id = 1,
      name =  "01",
      username = "io",
      email = "Sincere@april.biz",
      address = Address(
        street = "Kulas Light",
        suite = "Apt. 556",
        city = "Gwenborough",
        zipcode = "92998-3874",
        geo = Geo(
          lat = "-37.3159",
          lng = "81.1496"
        )
      ),
      phone = "1-770-712316-8031 x56442",
      website = "hildegard.org",
      company = Company(
        name = "Romaguera-Crona",
        catchPhrase = "Multi-layered client-server neural-net",
        bs = "harness real-time e-markets"
      )
    )
  )

  def getAll(): Future[List[User]] = Future.successful(storage.values.toList.sortBy(_.id))

  def get(id: Long): Future[Option[User]] = Future.successful(storage.get(id))

  def create(params: UserParams): Future[User] = params match {
    case UserParams(
      Some(name), Some(username), Some(email),
      Some(address), Some(phone), Some(website),
      Some(company)
    ) =>
      val user = User(
        idSeq.getAndIncrement(), name, username,
        email, address, phone, website, company)
      storage.put(user.id, user)
      Future.successful(user)
    case _ =>
      Future.failed(new IllegalArgumentException())
  }

  def update(id: Long, params: UserParams): Future[Option[User]] = {
    storage.get(id) match {
      case Some(user) =>
        val newUser = user.copy(
          name = params.name.getOrElse(user.name),
          username = params.username.getOrElse(user.username),
          email = params.email.getOrElse(user.email),
          address = params.address.getOrElse(user.address),
          phone = params.phone.getOrElse(user.phone),
          website = params.website.getOrElse(user.website),
          company = params.company.getOrElse(user.company)
        )
        storage.put(id, newUser)
        Future.successful(Some(newUser))
      case None =>
        Future.successful(None)
    }
  }

  def delete(id: Long): Future[Boolean] = Future.successful(storage.remove(id).nonEmpty)

}