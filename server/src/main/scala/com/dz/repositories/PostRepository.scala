package com.dz.repositories

import java.util.concurrent.atomic.AtomicLong

import com.dz.models.{Post, PostParams}

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

trait TraitPostRepository {
  def getAll(): Future[List[Post]]
  def get(id: Long): Future[Option[Post]]
  def create(params: PostParams): Future[Post]
  def update(id: Long, params: PostParams): Future[Option[Post]]
  def delete(id: Long): Future[Boolean]
  def getByOwner(id: Long): Future[List[Post]]
}

class PostRepository extends TraitPostRepository {
  private val storage: TrieMap[Long, Post] = TrieMap[Long, Post]()
  private val idSeq = new AtomicLong(1)

  //Тестовые данные
  storage.put(idSeq.getAndIncrement(), Post(1,1, "Title post 1", "body body body"))
  storage.put(idSeq.getAndIncrement(), Post(8,2, "Title post 2", "tra tra tra tra"))

  def getAll(): Future[List[Post]] = Future.successful(storage.values.toList.sortBy(_.id))

  def get(id: Long): Future[Option[Post]] = Future.successful(storage.get(id))

  def create(params: PostParams): Future[Post] = params match {
    case PostParams(Some(userId), Some(title), Some(body)) =>
      val post = Post(userId, idSeq.getAndIncrement(), title, body)
      storage.put(post.id, post)
      Future.successful(post)
    case _ =>
      Future.failed(new IllegalArgumentException())
  }

  def update(id: Long, params: PostParams): Future[Option[Post]] = {
    storage.get(id) match {
      case Some(post) =>
        val newPost = post.copy(
          userId = params.userId.getOrElse(post.userId),
          title = params.title.getOrElse(post.title),
          body = params.body.getOrElse(post.body)
        )
        storage.put(id, newPost)
        Future.successful(Some(newPost))
      case None =>
        Future.successful(None)
    }
  }

  def delete(id: Long): Future[Boolean] = Future.successful(storage.remove(id).nonEmpty)

  def getByOwner(id: Long): Future[List[Post]] =  Future.successful(storage.values.toList.filter(_.userId==id).sortBy(_.id))
}