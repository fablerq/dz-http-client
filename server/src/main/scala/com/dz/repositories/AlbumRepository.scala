package com.dz.repositories

import java.util.concurrent.atomic.AtomicLong

import com.dz.models.{Album, AlbumParams}

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

trait TraitAlbumRepository {
  def getAll(): Future[List[Album]]
  def get(id: Long): Future[Option[Album]]
  def create(params: AlbumParams): Future[Album]
  def update(id: Long, params: AlbumParams): Future[Option[Album]]
  def delete(id: Long): Future[Boolean]
  def getByOwner(id: Long): Future[List[Album]]
}

class AlbumRepository extends TraitAlbumRepository {
  private val storage: TrieMap[Long, Album] = TrieMap[Long, Album]()
  private val idSeq = new AtomicLong(1)

  //Тестовые данные
  storage.put(idSeq.getAndIncrement(), Album(1,1, "Best Album"))
  storage.put(idSeq.getAndIncrement(), Album(6,2, "Album Forever"))

  def getAll(): Future[List[Album]] = Future.successful(storage.values.toList.sortBy(_.id))

  def get(id: Long): Future[Option[Album]] = Future.successful(storage.get(id))

  def create(params: AlbumParams): Future[Album] = params match {
    case AlbumParams(Some(userId), Some(title)) =>
      val album = Album(userId, idSeq.getAndIncrement(), title)
      storage.put(album.id, album)
      Future.successful(album)
    case _ =>
      Future.failed(new IllegalArgumentException())
  }

  def update(id: Long, params: AlbumParams): Future[Option[Album]] = {
    storage.get(id) match {
      case Some(album) =>
        val newAlbum = album.copy(
          userId = params.userId.getOrElse(album.userId),
          title = params.title.getOrElse(album.title),
        )
        storage.put(id, newAlbum)
        Future.successful(Some(newAlbum))
      case None =>
        Future.successful(None)
    }
  }

  def delete(id: Long): Future[Boolean] = Future.successful(storage.remove(id).nonEmpty)

  def getByOwner(id: Long): Future[List[Album]] = Future.successful(storage.values.toList.filter(_.userId==id).sortBy(_.id))
}
