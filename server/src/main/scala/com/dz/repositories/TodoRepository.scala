package com.dz.repositories

import java.util.concurrent.atomic.AtomicLong

import com.dz.models.{Todo, TodoParams}

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

trait TraitTodoRepository {
  def getAll(): Future[List[Todo]]
  def get(id: Long): Future[Option[Todo]]
  def create(params: TodoParams): Future[Todo]
  def update(id: Long, params: TodoParams): Future[Option[Todo]]
  def delete(id: Long): Future[Boolean]
  def getByOwner(id: Long): Future[List[Todo]]
}

class TodoRepository extends TraitTodoRepository {
  private val storage: TrieMap[Long, Todo] = TrieMap[Long, Todo]()
  private val idSeq = new AtomicLong(1)

  //Тестовые данные
  storage.put(idSeq.getAndIncrement(), Todo(1,1, "Complete task A", false))
  storage.put(idSeq.getAndIncrement(), Todo(5,2, "Complete task B", true))

  def getAll(): Future[List[Todo]] = Future.successful(storage.values.toList.sortBy(_.id))

  def get(id: Long): Future[Option[Todo]] = Future.successful(storage.get(id))

  def create(params: TodoParams): Future[Todo] = params match {
    case TodoParams(Some(userId), Some(title), Some(completed)) =>
      val todo = Todo(userId, idSeq.getAndIncrement(), title, completed)
      storage.put(todo.id, todo)
      Future.successful(todo)
    case _ =>
      Future.failed(new IllegalArgumentException())
  }

  def update(id: Long, params: TodoParams): Future[Option[Todo]] = {
    storage.get(id) match {
      case Some(todo) =>
        val newTodo = todo.copy(
          userId = params.userId.getOrElse(todo.userId),
          title = params.title.getOrElse(todo.title),
          completed = params.completed.getOrElse(todo.completed)
        )
        storage.put(id, newTodo)
        Future.successful(Some(newTodo))
      case None =>
        Future.successful(None)
    }
  }

  def delete(id: Long): Future[Boolean] = Future.successful(storage.remove(id).nonEmpty)

  def getByOwner(id: Long): Future[List[Todo]] =  Future.successful(storage.values.toList.filter(_.userId==id).sortBy(_.id))
}
