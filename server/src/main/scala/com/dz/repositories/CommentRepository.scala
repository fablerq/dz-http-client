package com.dz.repositories

import java.util.concurrent.atomic.AtomicLong
import com.dz.models.{Comment, CommentParams}
import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

trait TraitCommentRepository {
  def getAll(): Future[List[Comment]]
  def get(id: Long): Future[Option[Comment]]
  def create(params: CommentParams): Future[Comment]
  def update(id: Long, params: CommentParams): Future[Option[Comment]]
  def delete(id: Long): Future[Boolean]
  def getByOwner(id: Long): Future[List[Comment]]
}

class CommentRepository extends TraitCommentRepository {
  private val storage: TrieMap[Long, Comment] = TrieMap[Long, Comment]()
  private val idSeq = new AtomicLong(1)

  //Тестовые данные
  storage.put(idSeq.getAndIncrement(), Comment(1,1, "Title 1", "qwe@ew.ru", "good service"))
  storage.put(idSeq.getAndIncrement(), Comment(13,2, "Title 2", "qewfwe@ew.ru", "ssss"))

  def getAll(): Future[List[Comment]] = Future.successful(storage.values.toList.sortBy(_.id))

  def get(id: Long): Future[Option[Comment]] = Future.successful(storage.get(id))

  def create(params: CommentParams): Future[Comment] = params match {
    case CommentParams(Some(postId), Some(name), Some(email), Some(body)) =>
      val comment = Comment(postId, idSeq.getAndIncrement(), name, email, body)
      storage.put(comment.id, comment)
      Future.successful(comment)
    case _ =>
      Future.failed(new IllegalArgumentException())
  }

  def update(id: Long, params: CommentParams): Future[Option[Comment]] = {
    storage.get(id) match {
      case Some(comment) =>
        val newComment = comment.copy(
          postId = params.postId.getOrElse(comment.postId),
          name = params.name.getOrElse(comment.name),
          email = params.email.getOrElse(comment.email),
          body = params.body.getOrElse(comment.body)
        )
        storage.put(id, newComment)
        Future.successful(Some(newComment))
      case None =>
        Future.successful(None)
    }
  }

  def delete(id: Long): Future[Boolean] = Future.successful(storage.remove(id).nonEmpty)

  def getByOwner(id: Long): Future[List[Comment]] = Future.successful(storage.values.toList.filter(_.postId==id).sortBy(_.id))
}
