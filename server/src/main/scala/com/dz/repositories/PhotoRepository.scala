package com.dz.repositories

import java.util.concurrent.atomic.AtomicLong

import com.dz.models.{Photo, PhotoParams}

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

trait TraitPhotoRepository {
  def getAll(): Future[List[Photo]]
  def get(id: Long): Future[Option[Photo]]
  def create(params: PhotoParams): Future[Photo]
  def update(id: Long, params: PhotoParams): Future[Option[Photo]]
  def delete(id: Long): Future[Boolean]
  def getByOwner(id: Long): Future[List[Photo]]
}

class PhotoRepository extends TraitPhotoRepository {
  private val storage: TrieMap[Long, Photo] = TrieMap[Long, Photo]()
  private val idSeq = new AtomicLong(1)

  //Тестовые данные
  storage.put(idSeq.getAndIncrement(), Photo(1,1, "Title photo 1", "www.wefwef.rt", "wefwef.com"))
  storage.put(idSeq.getAndIncrement(), Photo(13,2, "Title photo 2", "www.wefewef.rt", "wefwef.fuj"))

  def getAll(): Future[List[Photo]] = Future.successful(storage.values.toList.sortBy(_.id))

  def get(id: Long): Future[Option[Photo]] = Future.successful(storage.get(id))

  def create(params: PhotoParams): Future[Photo] = params match {
    case PhotoParams(Some(albumId), Some(title), Some(url), Some(thumbnailUrl)) =>
      val photo = Photo(albumId, idSeq.getAndIncrement(), title, url, thumbnailUrl)
      storage.put(photo.id, photo)
      Future.successful(photo)
    case _ =>
      Future.failed(new IllegalArgumentException())
  }

  def update(id: Long, params: PhotoParams): Future[Option[Photo]] = {
    storage.get(id) match {
      case Some(photo) =>
        val newPhoto = photo.copy(
          albumId = params.albumId.getOrElse(photo.albumId),
          title = params.title.getOrElse(photo.title),
          url = params.url.getOrElse(photo.url),
          thumbnailUrl = params.thumbnailUrl.getOrElse(photo.thumbnailUrl)
        )
        storage.put(id, newPhoto)
        Future.successful(Some(newPhoto))
      case None =>
        Future.successful(None)
    }
  }

  def delete(id: Long): Future[Boolean] = Future.successful(storage.remove(id).nonEmpty)

  def getByOwner(id: Long): Future[List[Photo]] =  Future.successful(storage.values.toList.filter(_.albumId==id).sortBy(_.id))
}