package com.dz.routes

import com.dz.models.CommentParams
import com.dz.repositories.CommentRepository
import akka.http.scaladsl.server.Directives._
import com.dz.Json4sSupport._

class CommentRoutes(commentRepository: CommentRepository) {
  def routes = {
    path("comments") {
      get {
        complete(commentRepository.getAll())
      } ~
        post {
          entity(as[CommentParams]) { params =>
            complete(commentRepository.create(params))
          }
        }
    } ~
    path("comments" / IntNumber) { id =>
        get {
          complete(commentRepository.get(id))
        } ~
          patch {
            entity(as[CommentParams]) { params =>
              complete(commentRepository.update(id, params))
            }
          } ~
          delete {
            complete(commentRepository.delete(id))
          }
    } ~
    path("comments?userId=" / IntNumber) { id =>
      get {
        complete(commentRepository.getByOwner(id))
      }
    }
  }
}
