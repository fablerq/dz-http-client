package com.dz.routes

import akka.http.scaladsl.server.Directives._
import com.dz.models.PhotoParams
import com.dz.repositories.PhotoRepository
import com.dz.Json4sSupport._

class PhotoRoutes(photoRepository: PhotoRepository) {
  def routes = {
    path("photos") {
      get {
        complete(photoRepository.getAll())
      } ~
        post {
          entity(as[PhotoParams]) { params =>
            complete(photoRepository.create(params))
          }
        }
    } ~
    path("photos" / IntNumber) { id =>
        get {
          complete(photoRepository.get(id))
        } ~
          patch {
            entity(as[PhotoParams]) { params =>
              complete(photoRepository.update(id, params))
            }
          } ~
          delete {
            complete(photoRepository.delete(id))
          }
    } ~
    path("photos?albumId=" / IntNumber) { id =>
        get {
          complete(photoRepository.getByOwner(id))
        }
    }
  }
}