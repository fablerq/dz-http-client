package com.dz.routes

import akka.http.scaladsl.server.Directives._
import com.dz.repositories._

class SummaryOfRoutes {
  val userRoutes = new UserRoutes(new UserRepository)
  val postRoutes = new PostRoutes(new PostRepository)
  val commentRoutes = new CommentRoutes(new CommentRepository)
  val albumRoutes = new AlbumRoutes(new AlbumRepository)
  val todoRoutes = new TodoRoutes(new TodoRepository)
  val photoRoutes = new PhotoRoutes(new PhotoRepository)

  val routes = {
    pathPrefix("api") {
      albumRoutes.routes ~ postRoutes.routes ~ commentRoutes.routes ~
      userRoutes.routes ~ todoRoutes.routes ~ photoRoutes.routes
    }
  }

}
