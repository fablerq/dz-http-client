package com.dz.routes

import akka.http.scaladsl.server.Directives._
import com.dz.models.TodoParams
import com.dz.repositories.TodoRepository
import com.dz.Json4sSupport._

class TodoRoutes(todoRepository: TodoRepository) {
  def routes = {
    path("todos") {
      get {
        complete(todoRepository.getAll())
      } ~
        post {
          entity(as[TodoParams]) { params =>
            complete(todoRepository.create(params))
          }
        }
    } ~
    path("todos" / IntNumber) { id =>
        get {
          complete(todoRepository.get(id))
        } ~
          patch {
            entity(as[TodoParams]) { params =>
              complete(todoRepository.update(id, params))
            }
          } ~
          delete {
            complete(todoRepository.delete(id))
          }
    } ~
    path("todos?userId=" / IntNumber) { id =>
        get {
          complete(todoRepository.getByOwner(id))
        }
    }
  }
}