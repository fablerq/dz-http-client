package com.dz.routes

import akka.http.scaladsl.server.Directives._
import com.dz.models.PostParams
import com.dz.repositories.PostRepository
import com.dz.Json4sSupport._

class PostRoutes(postRepository: PostRepository) {
  def routes = {
    path("posts") {
      get {
        complete(postRepository.getAll())
      } ~
        post {
          entity(as[PostParams]) { params =>
            complete(postRepository.create(params))
          }
        }
    } ~
    path("posts" / IntNumber) { id =>
        get {
          complete(postRepository.get(id))
        } ~
        patch {
            entity(as[PostParams]) { params =>
              complete(postRepository.update(id, params))
            }
        } ~
        delete {
            complete(postRepository.delete(id))
        }
    } ~
    path("posts?userId=" / IntNumber) { id =>
      get {
        complete(postRepository.getByOwner(id))
      }
    }
  }
}