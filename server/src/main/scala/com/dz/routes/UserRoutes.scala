package com.dz.routes

import com.dz.repositories.UserRepository
import akka.http.scaladsl.server.Directives._
import com.dz.models.UserParams
import com.dz.Json4sSupport._

class UserRoutes(userRepository: UserRepository) {
  def routes = {
    path("users") {
      get {
        complete(userRepository.getAll())
      } ~
        post {
          entity(as[UserParams]) { params =>
            complete(userRepository.create(params))
          }
        }
    } ~
    path("users" / IntNumber) { id =>
      get {
        complete(userRepository.get(id))
      } ~
      patch {
        entity(as[UserParams]) { params =>
          complete(userRepository.update(id, params))
        }
      } ~
      delete {
        complete(userRepository.delete(id))
      }
    }
  }
}