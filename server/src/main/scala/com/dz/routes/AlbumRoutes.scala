package com.dz.routes

import akka.http.scaladsl.server.Directives._
import com.dz.models.AlbumParams
import com.dz.repositories.AlbumRepository
import com.dz.Json4sSupport._

class AlbumRoutes(albumRepository: AlbumRepository) {
  def routes = {
    path("albums") {
      get {
        complete(albumRepository.getAll())
      } ~
        post {
          entity(as[AlbumParams]) { params =>
            complete(albumRepository.create(params))
          }
        }
    } ~
    path("albums" / IntNumber) { id =>
        get {
          complete(albumRepository.get(id))
        } ~
          patch {
            entity(as[AlbumParams]) { params =>
              complete(albumRepository.update(id, params))
            }
          } ~
          delete {
            complete(albumRepository.delete(id))
          }
    } ~
    path("albums?userId=" / IntNumber) { id =>
      get {
        complete(albumRepository.getByOwner(id))
      }
    }
  }
}