package com.dz.models

case class Album(
                  userId: Int,
                  id: Long,
                  title: String)
case class AlbumParams(
                  userId: Option[Int] = None,
                  title: Option[String] = None)
