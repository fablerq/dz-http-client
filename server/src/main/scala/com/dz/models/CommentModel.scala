package com.dz.models

case class Comment(
                    postId: Int,
                    id: Long,
                    name: String,
                    email: String,
                    body: String)
case class CommentParams(
                    postId: Option[Int] = None,
                    name: Option[String] = None,
                    email: Option[String] = None,
                    body: Option[String] = None)