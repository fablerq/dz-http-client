package com.dz.models

case class Photo(
                  albumId: Int,
                  id: Long,
                  title: String,
                  url: String,
                  thumbnailUrl: String)

case class PhotoParams(
                        albumId: Option[Int] = None,
                        title: Option[String] = None,
                        url: Option[String] = None,
                        thumbnailUrl: Option[String] = None)