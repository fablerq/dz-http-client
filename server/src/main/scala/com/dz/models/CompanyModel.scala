package com.dz.models

case class Company(
                    name: String,
                    catchPhrase: String,
                    bs: String)