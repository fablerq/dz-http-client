package com.dz.models

case class Address(
                    street: String,
                    suite: String,
                    city: String,
                    zipcode: String,
                    geo: Geo)