package com.dz.models

case class Geo(
                lat: String,
                lng: String)