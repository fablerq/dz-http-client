package com.dz.models

case class Post(
                 userId: Int,
                 id: Long,
                 title: String,
                 body: String)
case class PostParams(
                       userId: Option[Int] = None,
                       title: Option[String] = None,
                       body: Option[String] = None)
