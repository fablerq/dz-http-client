package com.dz.models

case class Todo(
                 userId: Int,
                 id: Long,
                 title: String,
                 completed: Boolean)

case class TodoParams(
                       userId: Option[Int] = None,
                       title: Option[String] = None,
                       completed: Option[Boolean] = None)