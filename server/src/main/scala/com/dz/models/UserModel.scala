package com.dz.models

case class User(
                 id: Long,
                 name: String,
                 username: String,
                 email: String,
                 address: Address,
                 phone: String,
                 website: String,
                 company: Company)
case class UserParams(
                       name: Option[String] = None,
                       username: Option[String] = None,
                       email: Option[String] = None,
                       address: Option[Address] = None,
                       phone: Option[String] = None,
                       website: Option[String] = None,
                       company: Option[Company] = None)