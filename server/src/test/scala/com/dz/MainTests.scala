package com.dz

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import org.scalatest._

class MainTests extends AsyncFlatSpec with Matchers with Json4sSupport {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val ec =  scala.concurrent.ExecutionContext.Implicits.global

  val client = new Client(Http().singleRequest(_))

  import client._

  //===================================================================================

  behavior of "Post"

  val post = Post(
    userId = 1,
    id = 1,
    title = "Title post 1",
    body = "body body body"
  )

  val createdpost = Post(
    userId = 1,
    id = 3,
    title = "John dead",
    body = "newbodyyy"
  )

  val postparams = PostParams(
    userId = Some(1),
    title = Some("John dead"),
    body = Some("newbodyyy")
  )

  val updatedpost = Post(
    userId = 1,
    id = 2,
    title = "John dead",
    body = "newbodyyy"
  )

  it should "get all posts" in {
    getAll[Post]("posts")
      .map { x => x should contain (post)}
  }

  it should "get first post" in {
    get[Post]("posts",1)
      .map { x => x shouldBe Some(post) }
  }

  it should "create new post" in {
    create[Post, PostParams]("posts", postparams)
      .map { x => x shouldBe Some(createdpost) }
  }

  it should "update post" in {
    update[Post, PostParams]("posts", 2, postparams)
      .map { x => x shouldBe Some(updatedpost) }
  }

  it should "delete post" in {
    delete("posts", 2)
      .map { x => x shouldBe true }
  }

  it should "return all single user posts" in {
    getByOwner[Post]("posts", "user",1)
      .map { x => x should contain (post)}
  }


  //===================================================================================

  behavior of "Album"

  val album = Album(
    userId = 1,
    id = 1,
    title = "Best Album"
  )

  val createdalbum = Album(
    userId = 1,
    id = 3,
    title = "Hey Hey Album"
  )

  val albumparams = AlbumParams(
    userId = Some(1),
    title = Some("Hey Hey Album")
  )

  val updatedalbum = Album(
    userId = 1,
    id = 2,
    title = "Hey Hey Album",
  )

  it should "get all albums" in {
    getAll[Album]("albums")
      .map { x => x should contain (album)}
  }

  it should "get first album" in {
    get[Album]("albums",1)
      .map { x => x shouldBe Some(album) }
  }

  it should "create new album" in {
    create[Album, AlbumParams]("albums", albumparams)
      .map { x => x shouldBe Some(createdalbum) }
  }

  it should "update album" in {
    update[Album, AlbumParams]("albums", 2, albumparams)
      .map { x => x shouldBe Some(updatedalbum) }
  }

  it should "delete album" in {
    delete("albums", 2)
      .map { x => x shouldBe true }
  }

  it should "return all single user albums" in {
    getByOwner[Album]("albums", "user",1)
      .map { x => x should contain (album)}
  }


  //===================================================================================

  behavior of "Comment"

  val comment = Comment(
    postId = 1,
    id = 1,
    name = "Title 1",
    email = "qwe@ew.ru",
    body = "good service"
  )

  val createdcomment = Comment(
    postId = 1,
    id = 3,
    name = "Title 3",
    email = "qwe@e2w.ru",
    body = "good service2"
  )

  val commentparams = CommentParams(
    postId = Some(1),
    name = Some("Title 3"),
    email = Some("qwe@e2w.ru"),
    body = Some("good service2")
  )

  val updatedcomment = Comment(
    postId = 1,
    id = 2,
    name = "Title 3",
    email = "qwe@e2w.ru",
    body = "good service2"
  )

  it should "get all comments" in {
    getAll[Comment]("comments")
      .map { x => x should contain (comment)}
  }

  it should "get first comment" in {
    get[Comment]("comments",1)
      .map { x => x shouldBe Some(comment) }
  }

  it should "create new comment" in {
    create[Comment, CommentParams]("comments", commentparams)
      .map { x => x shouldBe Some(createdcomment) }
  }

  it should "update comment" in {
    update[Comment, CommentParams]("comments", 2, commentparams)
      .map { x => x shouldBe Some(updatedcomment) }
  }

  it should "delete comment" in {
    delete("comments", 2)
      .map { x => x shouldBe true }
  }

  it should "return all single post comments" in {
    getByOwner[Comment]("comments", "post",1)
      .map { x => x should contain (comment)}
  }

  //===================================================================================

  behavior of "Photo"

  val photo = Photo(
    albumId = 1,
    id = 1,
    title = "Title photo 1",
    url = "www.wefwef.rt",
    thumbnailUrl = "wefwef.com"
  )

  val createdphoto = Photo(
    albumId = 1,
    id = 3,
    title = "Title photo 3",
    url = "xcvxv@rge.ru",
    thumbnailUrl = "vk.com"
  )

  val photoparams = PhotoParams(
    albumId = Some(1),
    title = Some("Title photo 3"),
    url = Some("xcvxv@rge.ru"),
    thumbnailUrl = Some("vk.com")
  )

  val updatedphoto = Photo(
    albumId = 1,
    id = 2,
    title = "Title photo 3",
    url = "xcvxv@rge.ru",
    thumbnailUrl = "vk.com"
  )

  it should "get all photos" in {
    getAll[Photo]("photos")
      .map { x => x should contain (photo)}
  }

  it should "get first photo" in {
    get[Photo]("photos",1)
      .map { x => x shouldBe Some(photo) }
  }

  it should "create new photo" in {
    create[Photo, PhotoParams]("photos", photoparams)
      .map { x => x shouldBe Some(createdphoto) }
  }

  it should "update photo" in {
    update[Photo, PhotoParams]("photos", 2, photoparams)
      .map { x => x shouldBe Some(updatedphoto) }
  }

  it should "delete photo" in {
    delete("photos", 2)
      .map { x => x shouldBe true }
  }

  it should "return all single album photos" in {
    getByOwner[Photo]("photos", "album",1)
      .map { x => x should contain (photo)}
  }

  //===================================================================================

  behavior of "Todo"

  val todo = Todo(
    userId = 1,
    id = 1,
    title = "Complete task A",
    completed = false
  )

  val createdtodo = Todo(
    userId = 1,
    id = 3,
    title = "Complete task C",
    completed = true
  )

  val todoparams = TodoParams(
    userId = Some(1),
    title = Some("Complete task C"),
    completed = Some(true),
  )

  val updatedtodo = Todo(
    userId = 1,
    id = 2,
    title = "Complete task C",
    completed = true
  )

  it should "get all todos" in {
    getAll[Todo]("todos")
      .map { x => x should contain (todo)}
  }

  it should "get first todo" in {
    get[Todo]("todos",1)
      .map { x => x shouldBe Some(todo) }
  }

  it should "create new todo" in {
    create[Todo, TodoParams]("todos", todoparams)
      .map { x => x shouldBe Some(createdtodo) }
  }

  it should "update todo" in {
    update[Todo, TodoParams]("todos", 2, todoparams)
      .map { x => x shouldBe Some(updatedtodo) }
  }

  it should "delete todo" in {
    delete("todos", 2)
      .map { x => x shouldBe true }
  }

  it should "return all single user todos" in {
    getByOwner[Todo]("todos", "user",1)
      .map { x => x should contain (todo)}
  }

  //===================================================================================

  behavior of "Users"

  val user = User(
    id = 1,
    name =  "Leanne Graham",
    username = "Bret",
    email = "Sincere@april.biz",
    address = Address(
      street = "Kulas Light",
      suite = "Apt. 556",
      city = "Gwenborough",
      zipcode = "92998-3874",
      geo = Geo(
        lat = "-37.3159",
        lng = "81.1496"
      )
    ),
    phone = "1-770-736-8031 x56442",
    website = "hildegard.org",
    company = Company(
      name = "Romaguera-Crona",
      catchPhrase = "Multi-layered client-server neural-net",
      bs = "harness real-time e-markets"
    )
  )

  val createduser = User(
    id = 3,
    name =  "Bratan bratanovich",
    username = "wef",
    email = "wefewf@wefwe.ri",
    address = Address(
      street = "Lenina",
      suite = "QWe. 12211",
      city = "Tokio",
      zipcode = "11111-11-11",
      geo = Geo(
        lat = "-99.3159",
        lng = "666.1496"
      )
    ),
    phone = "+7000 00 0 0 ",
    website = "google.org",
    company = Company(
      name = "Procfile",
      catchPhrase = "tra tra tra",
      bs = "mdems"
    )
  )

  val userparams = UserParams(
    name =  Some("Bratan bratanovich"),
    username = Some("wef"),
    email = Some("wefewf@wefwe.ri"),
    address = Some(Address(
      street = "Lenina",
      suite = "QWe. 12211",
      city = "Tokio",
      zipcode = "11111-11-11",
      geo = Geo(
        lat = "-99.3159",
        lng = "666.1496"
      )
    )),
    phone = Some("+7000 00 0 0 "),
    website = Some("google.org"),
    company = Some(Company(
      name = "Procfile",
      catchPhrase = "tra tra tra",
      bs = "mdems"
    ))
  )

  val updateduser = User(
    id = 3,
    name =  "Bratan bratanovich",
    username = "wef",
    email = "wefewf@wefwe.ri",
    address = Address(
      street = "Lenina",
      suite = "QWe. 12211",
      city = "Tokio",
      zipcode = "11111-11-11",
      geo = Geo(
        lat = "-99.3159",
        lng = "666.1496"
      )
    ),
    phone = "+7000 00 0 0 ",
    website = "google.org",
    company = Company(
      name = "Procfile",
      catchPhrase = "tra tra tra",
      bs = "mdems"
    )
  )

  it should "get all users" in {
    getAll[User]("users")
      .map { x => x should contain (user)}
  }

  it should "get first user" in {
    get[User]("users",1)
      .map { x => x shouldBe Some(user) }
  }

  it should "create new user" in {
    create[User, UserParams]("users", userparams)
      .map { x => x shouldBe Some(createduser) }
  }

  it should "update user" in {
    update[User, UserParams]("users", 3, userparams)
      .map { x => x shouldBe Some(updateduser) }
  }

  it should "delete user" in {
    delete("users", 9)
      .map { x => x shouldBe true }
  }

}
