package com.dz

import scala.concurrent.Future

trait ClientMethods {
  def getAll[A: Manifest](prefix: String): Future[List[A]]
  def get[A: Manifest](prefix: String, id: Int): Future[Option[A]]
  def create[A: Manifest, B <: AnyRef](prefix: String, params: B): Future[Option[A]]
  def update[A: Manifest, B <: AnyRef](prefix: String, id: Int, params: B): Future[Option[A]]
  def delete(prefix: String, id: Int): Future[Boolean]
  def getByOwner[A: Manifest](prefix: String, prefix2: String, id: Int): Future[List[A]]
}
