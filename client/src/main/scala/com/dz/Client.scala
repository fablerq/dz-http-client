package com.dz

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import org.json4s.jackson.Serialization.write
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import scala.concurrent.Future
import com.dz.Json4sSupport._

case class Post(
               userId: Int,
               id: Int,
               title: String,
               body: String)
case class PostParams(
               userId: Option[Int] = None,
               title: Option[String] = None,
               body: Option[String] = None)

case class Comment(
              postId: Int,
              id: Int,
              name: String,
              email: String,
              body: String)
case class CommentParams(
              postId: Option[Int] = None,
              name: Option[String] = None,
              email: Option[String] = None,
              body: Option[String] = None)

case class Album(
              userId: Int,
              id: Int,
              title: String)
case class AlbumParams(
              userId: Option[Int] = None,
              title: Option[String] = None)

case class Photo(
              albumId: Int,
              id: Int,
              title: String,
              url: String,
              thumbnailUrl: String)

case class PhotoParams(
              albumId: Option[Int] = None,
              title: Option[String] = None,
              url: Option[String] = None,
              thumbnailUrl: Option[String] = None)

case class Todo(
               userId: Int,
               id: Int,
               title: String,
               completed: Boolean)

case class TodoParams(
               userId: Option[Int] = None,
               title: Option[String] = None,
               completed: Option[Boolean] = None)

case class Geo(
              lat: String,
              lng: String)
case class Address(
              street: String,
              suite: String,
              city: String,
              zipcode: String,
              geo: Geo)
case class Company(
              name: String,
              catchPhrase: String,
              bs: String)
case class User(
                 id: Int,
                 name: String,
                 username: String,
                 email: String,
                 address: Address,
                 phone: String,
                 website: String,
                 company: Company)
case class UserParams(
                 name: Option[String] = None,
                 username: Option[String] = None,
                 email: Option[String] = None,
                 address: Option[Address] = None,
                 phone: Option[String] = None,
                 website: Option[String] = None,
                 company: Option[Company] = None)


class Client(makeRequest: HttpRequest => Future[HttpResponse]) extends ClientMethods {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  def getAll[A : Manifest](prefix: String): Future[List[A]] = {
    Http()
      .singleRequest(HttpRequest(
        HttpMethods.GET,
        Uri(s"http://localhost:8000/api/$prefix")))
      .flatMap(Unmarshal(_).to[List[A]])
  }

  def get[A : Manifest](prefix: String, id: Int): Future[Option[A]] = {
    Http()
      .singleRequest(HttpRequest(
        HttpMethods.GET,
        Uri(s"http://localhost:8000/api/$prefix/$id")))
      .flatMap(Unmarshal(_).to[Option[A]])
  }

  def create[A: Manifest, B <: AnyRef](prefix: String, params: B): Future[Option[A]] = {
    Http()
      .singleRequest(HttpRequest(
        HttpMethods.POST,
        Uri(s"http://localhost:8000/api/$prefix"),
        entity = HttpEntity(ContentTypes.`application/json`, write(params))
      ))
      .flatMap(Unmarshal(_).to[Option[A]])
  }

  def update[A : Manifest, B <: AnyRef](prefix: String, id: Int, params: B): Future[Option[A]] = {
    Http()
      .singleRequest(HttpRequest(
        HttpMethods.PATCH,
        Uri(s"http://localhost:8000/api/$prefix/$id"),
        entity = HttpEntity(ContentTypes.`application/json`, write(params))
      ))
      .flatMap(Unmarshal(_).to[Option[A]])
  }

  def delete(prefix: String, id: Int): Future[Boolean] = {
    Http()
      .singleRequest(HttpRequest(
        HttpMethods.DELETE,
        Uri(s"http://localhost:8000/api/$prefix/$id")))
      .collect {
        case x if x.status.intValue() == 200 => true
        case _ => false
      }
  }

  def getByOwner[A : Manifest](prefix: String, prefix2: String, id: Int): Future[List[A]]= {
    Http()
      .singleRequest(HttpRequest(
        HttpMethods.GET,
        Uri(s"http://localhost:8000/api/$prefix?${prefix2}Id=$id")))
      .flatMap(Unmarshal(_).to[List[A]])
  }
}
