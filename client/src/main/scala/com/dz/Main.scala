package com.dz

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

object Main extends App{
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

//  Client.getAll[Post]("posts")
//    .onComplete {
//      case Success(res) => println(res)
//      case Failure(_)   => sys.error("something wrong")
//    }

}
